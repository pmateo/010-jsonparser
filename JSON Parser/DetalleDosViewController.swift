//
//  DetalleDosViewController.swift
//  JSON Parser
//
//  Created by formador on 13/1/17.
//  Copyright © 2017 cice. All rights reserved.
//

import UIKit

class DetalleDosViewController: UIViewController {

    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var cuerpo: UITextView!
    @IBOutlet weak var firmas: UILabel!
    
    var itemDetalle: [String: String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(itemDetalle["title"])
        
        
        titulo.text = itemDetalle["title"]!
        cuerpo.text = itemDetalle["body"]!
        firmas.text = itemDetalle["sigs"]!
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
